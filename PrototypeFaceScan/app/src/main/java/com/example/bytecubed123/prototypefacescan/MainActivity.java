package com.example.bytecubed123.prototypefacescan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.luxand.FSDK;
public class MainActivity extends Activity {

    Button btnSearchButton;
    EditText txtShowFaces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize facial recog
        int status = FSDK.ActivateLibrary("Na8JAnopUk3dgwzuP/y3nwPRUd5tLDmbrkjYQ8rk3k8msH4nzvDcuH5MJQz2REJ506wrLtO77OoSXPOiYD0vZvFYnmnccQuty36m4I+/mG1cjfckfcSOsjq4y5ToRQJqotA2V0lGBya1TMhe5+UHmSSr0lBiCFbGUw/TIVKhz28=");

        if(status != FSDK.FSDKE_OK) {
            showErrorAndClose("FaceSDK activation failed", status);
        }
        else {
            FSDK.Initialize();

            // Lock orientation
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            btnSearchButton = (Button) findViewById(R.id.search_button);
            txtShowFaces = (EditText) findViewById(R.id.show_faces_text);
            txtShowFaces.setInputType(InputType.TYPE_NULL);
            btnSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    txtShowFaces.setText("found 0 faces");

                    try {
                        ImageView view = (ImageView) findViewById(R.id.imageView);
                        view.setImageResource(R.drawable.lonely_boy);
                        Integer numFaces = detectFaces(view);

                        String facesShown = "found " + numFaces + (numFaces != 1 ? " faces" : " face");
                        txtShowFaces.setText(facesShown);
                    } catch (Exception e) {
                        txtShowFaces.setText(e.getMessage());
                    }
                }
            });
        }
    }

    private Integer detectFaces(ImageView view) {

        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void showErrorAndClose(String error, int code) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(error + ": " + code)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                })
                .show();
    }
}


class FaceRectangle {
    public int x1, y1, x2, y2;
}

class ProcessImageAndDrawResults extends View {

    public ProcessImageAndDrawResults(Context context) {
        super(context);
    }
    @Override
    protected void onDraw(Canvas canvas) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) { //NOTE: the method can be implemented in Preview class

        return true;
    }
}